#!/bin/bash
#shellcheck disable=SC2034
# MKit - simple install helper
# See LICENSE file for copyright and license details.


#
# Add 'async' tag to build meta-data (true|false)
#
# If 'true' and during the version computation git branch is behind
# or ahead its remote counterpart, build meta-data (part after '+'
# sign) will contain word 'async'.
#
# This is to warn testers and developers when build was done after
# automatic rebase done by CI system.
#
# Defaults to 'true'.
#
MKIT_ASYNC=${MKIT_ASYNC:-true}

#
# Bump size (for vbump_? and release_?)
#
MKIT_BUMPSIZE=${MKIT_BUMPSIZE:-1}

#
# Debug mode (true|false)
#
MKIT_DEBUG=${MKIT_DEBUG:-false}

#
# Default deploy mode for files
#
MKIT_DEFAULT_MODE="644"

#
# Dry mode (true|false)
#
# Set to true to not install anything. Implies MKIT_DEBUG.
#
MKIT_DRY=${MKIT_DRY:-false}

#
# Is MKit running in CI (true|false)
#
# If MKit is running in CI, it's recommended to set this to 'true'
# to enable one or more useful warnings.
#
# Defaults to 'false'.
#
MKIT_IN_CI=${MKIT_IN_CI:-false}

#
# Path to mkit.ini
#
MKIT_INI=${MKIT_INI:-mkit.ini}

#
# Limit ini expansion depth
#
# To avoid endless loops, this value is subtracted each
# time ini() expands a reference; when zero is reached,
# no more expansions happen.
#
MKIT_INI_EXPAND=2

#
# Path to MKit local config and temp
#
# Typically hidden in project root, here MKit can
# save its temporary lists.
#
MKIT_LOCAL=${MKIT_LOCAL:-.mkit}

#
# Package name
#
# Used as base for tarball and in some default macros.
#
MKIT_PROJ_PKGNAME=""

#
# Add time-based ordinal tag to SemVer build data?
#
# Can be 'none', 'ctime' or 'btime'.
#
# If 'ctime', devel builds have also timestamp-based tag in format of
# `t%Y%m%d%H%M%S`, that is, a small character 't' followed by timestamp
# without non-digit characters.  The timestamps are in UTC, ie. timezones
# need not apply.  'btime' has the same format, except that it's derived
# from build time, while 'ctime' is from last commit's commit date.
#
# This helps with deploying development builds where packaging system
# is not SemVer-compliant and makes it hard to install arbitrary version.
# For example, old yum version (as of RHEL-6) will not let you install
# version that it deems older than is installed, making it hard to
# continually upgrade during active development.  While packaging
# systems have their own rules (and SemVer says both versions should be
# considered same) this tag will make it more likely to "win" the build
# you made later.
#
# Note that this does not affect clean builds (ie. builds from clean
# repo with HEAD corresponding to latest version tag.).
#
# Also note that 'btime' makes the version non-deterministic: merely
# initiating the build a second later will result in different version.
#
MKIT_TSTAMP=${MKIT_TSTAMP:-ctime}

#
# Upstream hash to add to build meta-data
#
# If set to non-empty string, it must represent a short-hash of
# upstream commit on which local branch was automatically rebased
# before build.
#
# If CI system automatically rebases branches, the resulting short-hash
# (appearing with `g` prefix) will be meaningless, and might be
# misleading -- testers and developers might see the build as coming
# from an unknown source.
#
# In that case, it's strongly recommended for CI to set this to the
# original pushed short-hash before the rebase.  Resulting meta-data
# will contain both hashes, allowing users to pair test results with
# original push.  The appearance of the extra hash will also warn
# about the fact that the rebase took place.
#
# For example, if developer pushed branch 'foo' that pointed to
# `5faf551`, and CI system rebased it, altering the HEAD to
# `bb4baa4`, by default the version would look like:
#
#     0.0.1+t202103011224.foo.gbb4baa4
#
# This would be meaningless for testing, as the `bb4baa4` hash is
# completely temporary and unknonwn.
#
# However, a conformant CI should set `MKIT_UPSTREAM=5faf551`, which
# will result in version like:
#
#     0.0.1+t202103011224.foo.gbb4baa4.u5faf551
#
# which still has technically correct git hash after `g`, but contains
# the original hash as pushed after `u`.
#
# Defaults to empty value.
#
MKIT_UPSTREAM=${MKIT_UPSTREAM:-}

#
# This MKit version
#
MKIT_VERSION=0.0.56
