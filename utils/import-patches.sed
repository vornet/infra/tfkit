#
# use this sed script to import patches back from repo
# where tfkit has been embedded
#
#  1. in the downstream repo, get patches using `git format-patch`,
#  2. apply this script to all files generated,
#  3. use `git am` to import patches back
#

/^\(--- a\|+++ b\|diff\)/ s:utils/tfkit/include:src/include:g
/^\(--- a\|+++ b\|diff\)/ s:utils/tfkit/runtests:src/runtests.skel:g
/^\(--- a\|+++ b\|diff\)/ s:utils/tfkit/tfkit.mk:src/tfkit.mk:g
/^index /           s:100755$:100644:
/^.TF_VERSION=/     s:TF_VERSION=.*$:TF_VERSION=__MKIT_PROJ_VERSION__:
