TFKIT
=====

A simple portable (or rather "movable") test framework.

 1. Copy (or `git submodule`) *test* sub-folder to your place
    where you want to run tests.

 2. Run your framework by `./tfkit/runtests`.

 3. Don't be surprised--you haven't added any tests yet :)

 4. Read tfkit/README.testing.md

 5. Start adding tests to `tests` folder
