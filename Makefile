# tfkit - Shellfu's movable test framework
# See LICENSE file for copyright and license details.

MKIT_DIR=utils/mkit
include $(MKIT_DIR)/mkit.mk

.PHONY: test
